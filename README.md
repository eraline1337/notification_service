# API for Mailing Service
**Stack: Django Rest Framework, Redis, Celery, PostgreSQL, Docker**

### Description
In the scope of this project you can: 
- Create a client database to store their phone numbers and tags.
- Create a scheduled mailing to send messages to the clients (filtered by tags) via 3rd party service.

### OpenAPI Specification
http://127.0.0.1:8000/docs/

### How to start the project
1) Clone the repository
```git clone git@gitlab.com:eraline1337/notification_service.git```

2) Create .env file in the project's root directory and fill it as in the example (you can use the values below):

```
REDIS_URL=redis://redis:6379/
DJANGO_SECRET_KEY='django-insecure-9xp9h0w8wu=bl_!%@16!4)x&o#mee3d57pc_exp@1maqxrre5k'
ENDPOINT=https://probe.fbrq.cloud/v1/send/
TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTIxODg1MTcsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImVyYWxpbmUifQ.cS2TIz7Wry6p37gvktCPPQm9-LO-SpX8gC9LHxMO9ZA
DB_ENGINE=django.db.backends.postgresql
DB_NAME=postgres
POSTGRES_USER=postgres 
POSTGRES_PASSWORD=postgres
DB_HOST=db # container name (db as default)
DB_PORT=5432
```

3) Run docker compose
```docker compose up --build```
4) Apply migrations
```docker container exec notification_service-api_app-1 python manage.py migrate```

Author: Konstantin K. https://github.com/eraline
