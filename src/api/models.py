from django.db import models

CHOICES = (('N', 'New'),
           ('F', 'Failed'),
           ('S', 'Sent'),
           ('A', 'Active'))


class Tag(models.Model):
    name = models.CharField(max_length=30)


class Mailing(models.Model):
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    text = models.TextField()
    operator_code = models.CharField(max_length=3, null=True)
    tags = models.ManyToManyField(Tag, related_name='mailings')


class Client(models.Model):
    phone_number = models.CharField(max_length=11)
    tags = models.ManyToManyField(Tag, related_name='clients')
    operator_code = models.CharField(max_length=3)
    timezone = models.CharField(max_length=30)


class Message(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1, choices=CHOICES)
    mailing = models.ForeignKey(
        Mailing,
        related_name='messages',
        on_delete=models.CASCADE)
    client = models.ForeignKey(
        Client,
        related_name='messages',
        on_delete=models.CASCADE)
    attempts = models.IntegerField(default=0)
