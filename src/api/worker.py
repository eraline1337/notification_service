import requests
from celery import shared_task
from django.utils import timezone

from notification_service.settings import ENDPOINT, TOKEN

from .models import Mailing, Message

headers = {
    "Authorization": f"Bearer {TOKEN}"
}


@shared_task
def send_message(message_id):
    message = Message.objects.get(pk=message_id)
    print(f'Sending {message.pk}')
    if message.mailing.end_datetime < timezone.now() or message.attempts >= 5:
        message.status = 'F'
        message.save()
        return
    if message.status == 'N':
        message.status = 'A'
        message.attempts += 1
        message.save()
        data = {
            "id": message.pk,
            "phone": message.client.phone_number,
            "text": message.mailing.text
        }

        response = requests.post(
            url=f'{ENDPOINT}{message.pk}',
            data=data,
            headers=headers
        )

        if response.status_code == 200:
            print(f'{message.pk} has been sent')
            message.status = 'S'
        else:
            print(f'{message.pk} has been failed')
            message.status = 'N'
        message.save()
    return


@shared_task
def start_mailing(mailing_id):
    print(mailing_id)
    mailing = Mailing.objects.get(id=mailing_id)
    messages = mailing.messages.all()
    while messages.filter(status__in=('A', 'N')).exists():
        messages = messages.filter(status__in=('A', 'N'))
        for message in messages:
            send_message.delay(message.pk)
    return
