# Generated by Django 3.2 on 2022-08-18 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_alter_mailing_operator_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.CharField(choices=[('N', 'New'), ('F', 'Failed'), ('S', 'Sent'), ('A', 'Active')], max_length=1),
        ),
    ]
