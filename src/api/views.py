import pandas as pd
from django.db.models import Count
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import action

from .models import Client, Mailing, Message
from .serializer import ClientSerializer, MailingSerializer
from .worker import start_mailing


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def create(self, request):
        response = super().create(request)
        mailing = Mailing.objects.get(pk=response.data['id'])
        if mailing.start_datetime > timezone.now():
            start_mailing.delay(mailing.pk)
        else:
            start_mailing.apply_async(
                eta=mailing.start_datetime, args=[mailing.pk])
        return response

    @action(methods=['get'], detail=False)
    def get_all_mailing_stats(self, request):
        data = Message.objects.values(
            'mailing__id', 'status').annotate(
            count=Count('id')).order_by('mailing__id')
        content = pd.DataFrame(data).to_string()
        return FileResponse(content, content_type='text/plain')

    @action(methods=['get'], detail=True)
    def get_mailing_stats(self, request, pk=None):
        mailing = get_object_or_404(Mailing, pk=pk)
        data = mailing.messages.all().values(
            'mailing__id', 'status').annotate(
            count=Count('id')).order_by('mailing__id')
        content = pd.DataFrame(data).to_string()
        return FileResponse(content, content_type='text/plain')
