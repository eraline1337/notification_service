import datetime as dt

from rest_framework import serializers

from .models import Client, Mailing, Message, Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')


class ClientSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, required=False)

    class Meta:
        model = Client
        fields = '__all__'

    def create(self, validated_data):
        if 'tags' in validated_data:
            tags = validated_data.pop('tags')
        else:
            tags = []
        client = Client.objects.create(**validated_data)
        batch = []
        for tag in tags:
            cur_tag, _ = Tag.objects.get_or_create(**tag)
            batch.append(cur_tag)
        client.tags.set(batch)
        return client

    def update(self, instance, validated_data):
        if 'tags' in validated_data:
            tags = validated_data.pop('tags')
        else:
            tags = []
        client = super().update(instance, validated_data)
        batch = []
        for tag in tags:
            cur_tag, _ = Tag.objects.get_or_create(**tag)
            batch.append(cur_tag)
        client.tags.set(batch)
        return client


class MailingSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, required=False)

    class Meta:
        model = Mailing
        fields = '__all__'

    def create(self, validated_data):
        tags = []
        if 'tags' in validated_data:
            tags = validated_data.pop('tags')
        mailing = Mailing.objects.create(**validated_data)
        filtered_clients = Client.objects.all()
        if tags:
            tags_batch = []
            for tag in tags:
                cur_tag, _ = Tag.objects.get_or_create(**tag)
                tags_batch.append(cur_tag)
            mailing.tags.set(tags_batch)
            filtered_clients = filtered_clients.filter(
                tags__in=mailing.tags.all())

        if mailing.operator_code:
            filtered_clients = filtered_clients.filter(
                operator_code=mailing.operator_code)

        batch = [Message(client=client, mailing=mailing, status='N')
                 for client in filtered_clients]
        Message.objects.bulk_create(batch)
        # start_mailing(mailing).delay(eta=mailing.start_datetime)
        # start_mailing.delay(mailing.pk)
        return mailing

    def update(self, instance, validated_data):
        tz = instance.start_datetime.tzinfo
        if instance.start_datetime < dt.datetime.now(tz):
            serializers.ValidationError(
                'You cannot update mailing that has been started already')
        tags = []
        if 'tags' in validated_data:
            tags = validated_data.pop('tags')
        mailing = super().update(instance, validated_data)
        filtered_clients = Client.objects.all()
        instance.messages.all().delete()
        if tags:
            tags_batch = []
            for tag in tags:
                cur_tag, _ = Tag.objects.get_or_create(**tag)
                tags_batch.append(cur_tag)
            mailing.tags.set(tags_batch)
            filtered_clients = filtered_clients.filter(
                tags__in=mailing.tags.all())
        else:
            mailing.tags.set([])
        if mailing.operator_code:
            filtered_clients = filtered_clients.filter(
                operator_code=mailing.operator_code)
        batch = [Message(client=client, mailing=mailing, status='N')
                 for client in filtered_clients]
        Message.objects.bulk_create(batch)
        # start_mailing(mailing).delay(eta=mailing.start_datetime)
        return mailing
