from datetime import timedelta
from urllib import response
from django.utils import timezone
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from api.models import Client, Mailing, Message

client = APIClient()


class ClientTests(APITestCase):
    def test_create_client_without_tags(self):
        path = reverse('client-list')
        data = {
            "phone_number": "79991234567",
            "operator_code": "999",
            "timezone": "Europe/Moscow"
        }
        response = self.client.post(path=path, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.count(), 1)
    
    def test_create_client_with_tags(self):
        path = reverse('client-list')
        data = {
            "phone_number": "79991234568",
            "operator_code": "999",
            "timezone": "Europe/Moscow",
            "tags":[{"name":"young"},
                    {"name":"Saint-Petersburg"},
                    {"name":"has_kids"}]
        }
        response = self.client.post(path=path, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.count(), 1)
        self.assertEqual(
            Client.objects.get(phone_number='79991234568').tags.count(), 3)


class MailingTests(APITestCase):
    def test_mailing(self):
        path = reverse('mailing-list')
        data = {
            "text":"Test Mailing",
            "start_datetime":timezone.now(),
            "end_datetime":timezone.now() + timedelta(minutes=10)
        }
        response = self.client.post(path=path, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Mailing.objects.count(), 1)
        print(f'Clients: {Client.objects.count()}')
        print(f'Messages: {Message.objects.count()}')
