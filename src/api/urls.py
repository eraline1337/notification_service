from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'client', views.ClientViewSet)
router.register(r'mailing', views.MailingViewSet)

urlpatterns = [
    path('', include(router.urls))
]
